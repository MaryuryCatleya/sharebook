from rest_framework.relations import PrimaryKeyRelatedField
from sharebookApp.models.book import Book
from sharebookApp.models.grade import Grade
from sharebookApp.models.editorial import Editorial
from rest_framework import serializers


class BookSerializer(serializers.ModelSerializer):
    editorial = PrimaryKeyRelatedField(queryset=Editorial.objects.all())
    

    class Meta:
        model = Book
        fields = ['id_book', 'isbn', 'title', 'language',
                  'price', 'state', 'editorial', 'grade']
