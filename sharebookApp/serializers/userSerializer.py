from rest_framework import serializers
from sharebookApp.models.user import User
#from sharebookApp.models.book import Book
#from sharebookApp.models.book import Editorial
#from sharebookApp.serializers.bookSerializer import BookSerializer

class UserSerializer(serializers.ModelSerializer):
    #book = BookSerializer()

    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'name','email', 'address', 'phone']

    def create(self, validated_data):
        #bookData = validated_data.pop('book')
        userInstance = User.objects.create(**validated_data)
        #Book.objects.create(user=userInstance, **bookData)
        return userInstance

    def to_representation(self, obj):
        user = User.objects.get(id=obj.id)
        #book = Book.objects.get(user=obj.id)
        #editorial = Editorial.objects.get(editorial=obj.id)

        return {
            'id': user.id,
            'username': user.username,
            'name': user.name,
            'email': user.email,
            'address': user.address,
            'phone': user.phone,
        }
